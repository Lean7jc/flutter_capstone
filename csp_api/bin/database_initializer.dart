import 'package:sqlite3/sqlite3.dart';

import './portable_database.dart';

void initializePortableDatabase() {
    db.execute('''
        CREATE TABLE users (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            email TEXT NOT NULL,
            password TEXT NOT NULL,
            designation TEXT NOT NULL
        );

        CREATE TABLE projects (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            description TEXT NOT NULL,
            createdBy INTEGER NOT NULL,
            assignedTo INTEGER
        );

        CREATE TABLE tasks (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            title TEXT NOT NULL,
            description TEXT NOT NULL,
            imageLocation TEXT,
            status TEXT NOT NULL,
            projectId INTEGER NOT NULL,
            assignedTo INTEGER
        )
    ''');
    
    // Valid values for tasks.status are OPEN, PENDING, ONGOING, COMPLETED, REJECTED and ACCEPTED.
    // The tasks.status field is set to TEXT data type since there is no ENUM data type in SQLite.

    final PreparedStatement userStatement = db.prepare('INSERT INTO users (id, email, password, designation) VALUES (?, ?, ?, ?)');

    userStatement.execute(['1', 'contractor@gmail.com', '\$2b\$10\$HR4B3sVRcGSOlrPY5HMOcecZABMAvzL96R.WaUJC6E7fVH7RRlUW2', 'contractor']);
    userStatement.execute(['2', 'subcontractor@gmail.com', '\$2b\$10\$MjOLAejkUbJvql31YXlBWO8qLG.L.hG.5jKoH/aUX7eoxnKR9TyE2', 'subcontractor']);
    userStatement.execute(['3', 'assembly-team@gmail.com', '\$2b\$10\$0iDC6Ce0ftCykT3.Lf1rZuLdUwub4wbHYyLsWiDyT5YLdfBwEOqQ2', 'assembly-team']);
    userStatement.dispose();

    final PreparedStatement projectStatement = db.prepare('INSERT INTO projects (id, name, description, createdBy, assignedTo) VALUES (?, ?, ?, ?, ?)');
    
    projectStatement.execute([1, 'House Construction', 'House to be built in Munich.', 1, null]);
    projectStatement.dispose();

    final PreparedStatement taskStatement = db.prepare('INSERT INTO tasks (id, title, description, status, projectId, assignedTo) VALUES (?, ?, ?, ?, ?, ?)');

    taskStatement.execute([1, 'Retrieve materials.', 'Get the materials to be used for construction.', 'Pending', 1, 3]);
    taskStatement.dispose();
}