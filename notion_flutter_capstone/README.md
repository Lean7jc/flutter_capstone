# Flutter Capstone Project

## Packages:

- **cupertino_icons: ^1.0.2**
Link: [https://pub.dev/packages/cupertino_icons](https://pub.dev/packages/cupertino_icons)

    This is an asset repo containing the default set of icon assets used by Flutter's Cupertino widgets.

- **email_validator: '^2.0.1'**
Link: [https://pub.dev/packages/email_validator](https://pub.dev/packages/email_validator)

    A simple (but correct) Dart class for validating email addresses without using RegEx. Can also be used to validate emails within Flutter apps (see Flutter email validation).

- **flutter_dotenv: ^5.0.0**
Link: [https://pub.dev/packages/flutter_dotenv](https://pub.dev/packages/flutter_dotenv)

    Load configuration at runtime from a .env file which can be used throughout the application.

- **image_picker: ^0.8.0+3**
Link: [https://pub.dev/packages/image_picker/changelog](https://pub.dev/packages/image_picker/changelog)

    A Flutter plugin for iOS and Android for picking images from the image library, and taking new pictures with the camera.

- **http: ^0.13.3** 
Link: [https://pub.dev/packages/http](https://pub.dev/packages/http)
    - This package contains a set of high-level functions and classes that make it easy to consume HTTP resources. It's multi-platform, and supports mobile, desktop, and the browser.
- **provider: ^5.0.0**
Link: [https://pub.dev/packages/provider/versions/5.0.0-nullsafety.5](https://pub.dev/packages/provider/versions/5.0.0-nullsafety.5)

    A wrapper around InheritedWidget to make them easier to use and more reusable.

    By using provider instead of manually writing InheritedWidget, you get:

    - simplified allocation/disposal of resources
    - lazy-loading
    - a largely reduced boilerplate over making a new class every time
    - devtools friendly
    - a common way to consume these InheritedWidgets (See Provider.of/Consumer/Selector)
    - increased scalability for classes with a listening mechanism that grows exponentially in complexity (such as ChangeNotifier, which is O(N²) for dispatching notifications).

- **shared_preferences: ^2.0.6**
Link: [https://pub.dev/packages/shared_preferences](https://pub.dev/packages/shared_preferences)
    - Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.). Data may be persisted to disk asynchronously, and there is no guarantee that writes will be persisted to disk after returning, so this plugin must not be used for storing critical data.

# Flutter Version:

### Flutter v3.25.0

[Flutter - Beautiful native apps in record time](https://flutter.dev/?gclid=CjwKCAjw64eJBhAGEiwABr9o2GySKj5gxUGkE_BRPj9voep5-DI-HaNrqDScPF25PrdAMj-RnTO8mBoCEcAQAvD_BwE&gclsrc=aw.ds)

# Features:

- login for user types
- A list of projects can show
- a project can be assigned
- project task list can be show
- can add a new project
- When the new project is created and assigned there is an account for the subcontractor
- In creation of task it can be assign to a team
- When the task is created there's another user type in the system for a assembly team
- In assembly team account it can assigned task  ongoing by clicking start
- When the task is finish it can be tagged as finish
- The contractor can the finished work
- The task can be accepted or rejected

# Setup Guide

1. Open the terminal your VS code or separate software 
2. type `git clone` in your terminal then paste the link in your bitbucket when you click the clone button in your repository `git clone [https://Lean7jc@bitbucket.org/Lean7jc/flutter_capstone.git](https://Lean7jc@bitbucket.org/Lean7jc/flutter_capstone.git)`
3. To get to the folder type `cd <app name>` in your terminal
4. In order to get the necessary type `flutter pub get` (name of the package) to install the all the packages that you need in your `pubspec.yml`
5. Type `flutter run` in your terminal to run the cloned project.

# Package Installation

1. Go to pubspec.yml in your then type the packages that you need (example: `shared_preferences: ^2.0.6` and make sure that the packages is properly indented and then ctrl + s to get the packages  
****