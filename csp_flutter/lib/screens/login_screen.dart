import 'dart:async';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '/utils/api.dart';
import '/models/user.dart';
import '/providers/user_provider.dart';

class LoginScreen extends StatefulWidget {
    @override
    _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
    Future<User>? _futureLogin;

    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffPasswordController = TextEditingController();

    void login(BuildContext context) {
        setState(() {
            _futureLogin = API().login(
                email: _tffEmailController.text,
                password: _tffPasswordController.text
            ).catchError((error) {
                showSnackBar(context, error.message);
            });
        });

    }

     void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = new SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000),
        );

        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }


    void goToHomepage(data) async {
        User user = data as User;

        final prefs = await SharedPreferences.getInstance();
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setDesignation = Provider.of<UserProvider>(context, listen: false).setDesignation;

        if (user.accessToken != null) {
            setAccessToken(user.accessToken);
            setDesignation(user.designation);

            prefs.setString('designation', user.designation!);

            Navigator.pushReplacementNamed(context, '/project-list');
        } else {
            showSnackBar(context, 'User not found.');
        }
    }

    @override
    Widget build(BuildContext context) {
       
        Widget txtEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            textInputAction: TextInputAction.next,
            validator: (email){
                if (email == null || email.isEmpty){
                    return 'The Email must be provided';
                }
                // used email validator against the errors that will be made by the users
                else if (EmailValidator.validate(email) == false){
                    return 'A valid email must be provided.';
                }
                return null;
            },
        );

        Widget txtPassword = TextFormField(
            decoration: InputDecoration(labelText: 'Password'),
            obscureText: true,
            controller: _tffPasswordController,
            textInputAction: TextInputAction.done,
            validator:  (password){
                bool isValid = password != null && password.isNotEmpty;
                return (isValid) ? null : 'The password must be provide';
            }
        );

        Widget btnSubmit = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 10.0),
            
            child: ElevatedButton(
                onPressed: () { 
                    if (_formKey.currentState!.validate()) {
                        login(context);
                    } else {
                        showSnackBar(context, 'Form validation failed. Check input and try again.');
                    }
                }, 
                child: Text('Login'),
                
            )
        );

       Widget lblAppTitle = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 60.0, bottom: 60.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    // Add styles to the texts below (size 30, bold font weight, red color on Login text).
                    Text(
                        'Project Management',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold )
                    ),
                    Text(
                        'Login',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.red)
                    )
                ]
            )
        );

        Widget imgLogo = Expanded(
            child: Padding(
                padding: EdgeInsets.only(bottom: 16.0),
                child: Align(
                    // Align the image according to shown sample.
                    child: Image.asset('assets/ffuf-logo.png', width: 100)
                )
            )
        );

        Widget formLogin = Form(
            key: _formKey,
            child: Column(
                children: [
                    lblAppTitle,
                    txtEmail,
                    txtPassword,
                    btnSubmit,
                    imgLogo
                ]
            )
        );

        Widget loginView = FutureBuilder(
            future: _futureLogin,
            builder: (context, snapshot) {
                if (_futureLogin == null) {
                    return formLogin;
                } else if (snapshot.hasError == true) {
                    return formLogin;
                } else if (snapshot.hasData == true) {
                    Timer(Duration(milliseconds: 1), () {
                        goToHomepage(snapshot.data);
                    });
                    // if wrong inputs of email and password will return in the Loginscreen 
                    return formLogin;
                }

                return Center(
                    child: CircularProgressIndicator()
                );
            }
        );

        return Scaffold(
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 30.0, left: 36.0, right: 36.0),
                child: loginView
            )
        );
    }
}